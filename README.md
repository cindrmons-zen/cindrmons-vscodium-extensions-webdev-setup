# cindrmons-vscodium-extensions (webdev-environment edition)

> Additional VSCode/ium extensions on top of the [main essentials](https://gitlab.com/cindrmons-zen/cindrmons-vscodium-extensions-essentials).

> I dev using tailwind, vue, and hugo most of the time, so these are the extensions to improve my workflow. Also included GraphQL just in case I use it.

## Tailwind CSS Tools

> Tailwind is awesome, now, let's make it awesomer with these tools!

- [Tailwind CSS Intellisense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)
  - This is just basic intellisense if ever you're planning to use Tailwind!
- [Tailwind Docs](https://marketplace.visualstudio.com/items?itemName=austenc.tailwind-docs)
  - This is also necessary, if you're unsure of which tailwindd thingy you need for your particular project. Remember, docs is just as good as asking questions (or searching for them) in stackoverflow or ~~Google~~ DuckDuckGo.

## Vue Tools

> I love vue as well, so I use these tools for vue use

- [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
  - An important tool to have syntax highlighting, and other necessary stuff if ever you're working with VueJS!
- [Vue Snippets](https://marketplace.visualstudio.com/items?itemName=hollowtree.vue-snippets)
  - Additional snippets tool if ever you want Vue Autocomplete in your workflow.

## Hugo Tools

> I use hugo too, and love how fast it is in terms of static site generation, yet it is hard to find good support for these. But I think this sort of helps when you're developing with hugo.

- [Hugo Helper](https://marketplace.visualstudio.com/items?itemName=rusnasonov.vscode-hugo)
  - Just a helpful tool when managing your hugo site.
- [Hugo Language and Syntax Support](https://marketplace.visualstudio.com/items?itemName=budparr.language-hugo-vscode)
  - It is really hard to find hugo support for VSCode/ium, and this is the best I can find so far, as this gives out proper syntax highlighting for hugo, and additional hugo snippets when coding in hugo.
- [Hugo Shortcode Syntax Highlighting](https://marketplace.visualstudio.com/items?itemName=kaellarkin.hugo-shortcode-syntax)
  - Additional support for Hugo if ever the first one wouldn't suffice.

## GraphQL

- [GraphQL](https://marketplace.visualstudio.com/items?itemName=graphql.vscode-graphql)
  - Planning to use GraphQL in future projects, really nice to use and have, in terms of managing databases and APIs
