# Change Log

All notable changes to the "cindrmons-vscodium-extensions-webdev-setup" extension pack will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [1.0.1] - 2021-11-22

## Added

- Core HTML Extensions from `cindrmons-vscodium-extensions-essentials`

## [1.0.0] - 2021-05-31

### Added

- Existing Extensions from old git repo

### Removed

- Essential Extensions (which was already setup [here](https://gitlab.com/cindrmons-zen/cindrmons-vscodium-extensions-essentials))
